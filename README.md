# Prime Number Identifier Lambda Function

This AWS Lambda function reads a list of integers from a CSV file stored in an S3 bucket and identifies which numbers are prime.

## Demo
[Demo URL](https://t9uf4aej79.execute-api.us-east-1.amazonaws.com/prod/find-primes-in-range)

## Features

- Reads numerical data from a CSV file (prime-candidates.csv) in an S3 bucket.
- Determines prime numbers from the list of integers.
- Returns a list of prime numbers in JSON format.

## Requirements

- Rust Programming Language
- Cargo package manager
- AWS CLI configured with appropriate permissions
- AWS account with access to Lambda and S3 services

## Lambda function

### Data Processing

The Lambda function performs the following steps to process the data:

- **Retrieve CSV**: The function fetches the `prime-candidates.csv` file from an S3 bucket named `find-prime-bucket`.
- **Read Data**: It reads the CSV data into memory, parsing the contents into a structure suitable for processing.
- **Parse Integers**: The function parses the CSV strings into integers. If a non-integer value is encountered, the function skips the invalid entry to prevent processing errors.
- **Determine Primality**: Each integer is evaluated to determine if it is a prime number using the `is_prime` function, which checks for factors other than 1 and the number itself.
- **Compile Results**: Prime numbers are compiled into a vector.
- **Generate Response**: A JSON response is generated, containing the list of prime numbers found in the CSV file. The response is then sent back to the client.


## S3 Bucket Setup
A S3 Bucket named find-prime-bucket is created, and a file named prime-candidates.csv was uploaded to the Bucket (Note: add permission AmazonS3FullAccess to the role who excutes this lambda function).

### prime-candidates.csv
```csv
number
2
3
4
5
6
7
8
9
10

```

## API Gateway Integration

An API Gateway is created and this lambda function is integrated into the API Gateway. The stage to which this lambda function attached is named prod, and the route is named /find-primes-in-range, which uses the GET method.

### Usage

```bash
curl https://t9uf4aej79.execute-api.us-east-1.amazonaws.com/prod/find-primes-in-range
```

### Response Format

The function responds with a JSON object containing a list of prime numbers identified from the CSV file:

```
{
  "primes": [2, 3, 5, 7]
}
```


## Screenshots
![Lambda function and API](find-primes-in-range/screenshots/1.png)

![S3 Bucket](find-primes-in-range/screenshots/2.png)

![Webpage](find-primes-in-range/screenshots/3.png)
