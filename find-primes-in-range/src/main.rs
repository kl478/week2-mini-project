use lambda_http::{run, service_fn, Body, Error, Request, Response};
use aws_sdk_s3::Client as S3Client;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use csv; 
use serde_json; 

#[derive(Debug, Deserialize)]
struct PrimeCandidate {
    number: i32,
}

#[derive(Serialize)]
struct PrimeResponse {
    primes: Vec<i32>,
}

fn is_prime(n: &i32) -> bool {
    let n = *n;
    if n <= 1 {
        return false;
    }
    for i in 2..=((n as f64).sqrt() as i32) {
        if n % i == 0 {
            return false;
        }
    }
    true
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let config = aws_config::load_from_env().await; 
    let client = S3Client::new(&config);

    let bucket = "find-prime-bucket";
    let key = "prime-candidates.csv";

    let resp = client.get_object().bucket(bucket).key(key).send().await?;
    let body = resp.body.collect().await?.into_bytes();
    let csv_content = String::from_utf8(body.to_vec()).expect("Found invalid UTF-8");

    let mut rdr = csv::Reader::from_reader(csv_content.as_bytes());
    let primes: Vec<i32> = rdr.deserialize()
        .filter_map(Result::ok)
        .map(|candidate: PrimeCandidate| candidate.number)
        .filter(|n| is_prime(n))
        .collect();

    let prime_response = PrimeResponse { primes };

    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&prime_response).unwrap().into())
        .expect("Failed to render response"))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::builder().with_default_directive(LevelFilter::INFO.into()).from_env_lossy())
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
